##############################
## Azure App Service - Main ##
##############################

# Create a Resource Group
resource "azurerm_resource_group" "ewbworkshopterraform" {
  name     = "ewbworkshopterraform-rg"
  location = var.location

  tags = {
    description = var.description
    environment = var.environment
    owner       = "bcsdemo"
    CostCentre  = var.costcentre
  }
}

# Create the App Service Plan
resource "azurerm_app_service_plan" "ewbworkshop-service-plan" {
  name                = "ewbworkshop-service-plan"
  location            = azurerm_resource_group.ewbworkshopterraform.location
  resource_group_name = azurerm_resource_group.ewbworkshopterraform.name
  kind                = "Windows"

  sku {
    tier = "Basic"
    size = "B1"
  }

  tags = {
    description = var.description
    environment = var.environment
    owner       = var.owner  
  }
}

# Create the App Service
resource "azurerm_app_service" "demoapp" {
  name                = "demoapp-web"
  location            = azurerm_resource_group.ewbworkshopterraform.location
  resource_group_name = azurerm_resource_group.ewbworkshopterraform.name
  app_service_plan_id = azurerm_app_service_plan.ewbworkshop-service-plan.id

  site_config {
    dotnet_framework_version = "v4.0"
  }

  
  tags = {
    description = var.description
    environment = var.environment
    owner       = var.owner  
  }
}

resource "azurerm_storage_account" "demostaticweb" {
  name                = "demostaticwebbcs"
  resource_group_name = azurerm_resource_group.ewbworkshopterraform.name

  location                 = azurerm_resource_group.ewbworkshopterraform.location
  account_tier             = "Standard"
  account_kind             = "StorageV2"
  account_replication_type = "LRS"
  static_website {
    index_document = "index.html"
  }

  tags = {
    environment = "dev"
  }
}

