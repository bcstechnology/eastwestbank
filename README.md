# East West Bank DevOps workshop #

**  Contacts **

* Jonathan Perez - jonathan.perez@bcstechnology.com.au
* Ian Lim - ian.lim@bcstechnology.com.au
* Ashish Agrawal - ashish.agrawal@bcstechnology.com.au


*You can do a git pull on the repository to get updated information if we update the files during the training.*

To get a copy on your local, run:

git clone git@bitbucket.org:bcstechnology/eastwestbank.git


**Day 1 Agenda**

Introduction – 10:00 – 10:15

Morning Session – 10:15 – 12:30
..- Iaas, Paas, Saas, and Serverless Architecture

Whiteboard Session – App Modernization – 12:30 – 1:00

Q&A + Lunch break – 1:00 – 2:00

(cont) Whiteboard Session – App Modernization – 2:00 – 3:00

Afternoon Session – 3:00 – 5:00

..-DevOps

..-DataOps for Managed SQL

Whiteboard Session – Continuous Delivery using Azure DevOps – 5:00 – 7:00

**Day 2 Agenda**


Recap - 9:00 – 9:10

DevOps Basics and Getting Started - 9:10-9:30

Azure Boards Overview - 9:10-9:30

Continuous Integration of a .Net Core Web Application – 10:00 – 11:30

Integration of Unit Test in Pipeline – 11:30 – 12:00

Integration of Static Code Analysis in the pipeline using SonarCloud – 12:00 – 12:30

Q&A + Lunch break – 12:30 – 1:30

Creating Infrastructure using Terraform – 1:30 – 2:30

Continuous Delivery and Deployment to Azure App Service – 2:30 – 3:30

PR Policy, Gated Delivery – 3:30 – 4:00

Test Management using Azure Test Plan – 4:00 – 5:00

DevOps Hackathon Challenge – 5:00-6:00


---




**references:**

What is DevOps

https://docs.microsoft.com/en-us/azure/devops/learn/what-is-devops

Devops checklist

https://docs.microsoft.com/en-us/azure/architecture/checklist/dev-ops

Basic Web application architecture

https://docs.microsoft.com/en-us/azure/architecture/reference-architectures/app-service-web-app/basic-web-app
